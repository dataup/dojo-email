FROM dataup/dojo-core:latest

VOLUME ["/app"]

ADD . /tmp/app
RUN pip install /tmp/app

# DEVELOPMENT ONLY
# ADD .cache/dojo /dojo
# RUN pip install -e /dojo --upgrade
# DEVELOPMENT ONLY

CMD ["/bin/bash"]